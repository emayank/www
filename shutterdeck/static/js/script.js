var sdcdn = "https://s3.amazonaws.com/sdcdn"

$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

function showMessage(message) {
    $alert = $('<div class="alert alert-danger alert-dismissible" role="alert">' +
    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
    '<span aria-hidden="true">&times;</span>' +
    '</button>' +
    message +
    '</div>');
    $('#message').html($alert);
}

function registerHandler() {
    $('#register-btn').on('click', function(e){
        if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
        var $form = $(this).parents('form');
        var $target = $(e.target);
        var data = $form.serializeObject();
        data = JSON.stringify(data);
        var url = $form.attr('action');
        var method = $form.attr('method');
        var $i = $('<i/>');
        $i.addClass("fa fa-refresh fa-spin");
        $target.append($i)
        $.ajax({
            'url': url,
            'method': method,
            'contentType': 'application/json; charset=utf-8',
            'dataType': 'json',
            'data': data
        }).done(function(data){
            $i.fadeOut('slow', function(){
                $i.remove();
            });
            if (data.status === 'success') {
                location.href = '/home';
            } else if (data.status === 'error') {
                showMessage(data.message);
            }
        });
        return false;
    });
}

function loginHandler() {
    $('#login-btn').on('click', function(e){
        if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
        var $form = $(this).parents('form');
        var $target = $(e.target);
        var data = $form.serializeObject();
        data = JSON.stringify(data);
        var url = $form.attr('action');
        var method = $form.attr('method');
        var $i = $('<i/>');
        $i.addClass("fa fa-refresh fa-spin");
        $target.append($i)
        $.ajax({
            'url': url,
            'method': method,
            'contentType': 'application/json; charset=utf-8',
            'dataType': 'json',
            'data': data
        }).done(function(data){
            $i.fadeOut('slow', function(){
                $i.remove();
            });
            if (data.status === 'success') {
                location.href = '/home';
            } else if (data.status === 'error') {
                showMessage(data.message);
            }
        }).error(function(data){
            $i.remove();
        });
        return false;
    });
}

function uploadHandler() {
    $('#fileupload').fileupload({
        url: '/photo/upload',
        sequentialUploads: true,
        add: function (e, data) {
            $.each(data.files, function (index, file) {
                $context = $('<div class="uploaded"></div>').appendTo('#uploaded-row');
                data.context = $context;
                data.submit();
            });
        },
        send: function (e, data) {
            data.context.html('<div><i class="fa fa-refresh fa-spin fa-3x"></i></div>');
        },
        done: function (e, data) {
            $div = $('<div/>');
            $i = $('<i class="fa fa-check fa-3x"></i>').appendTo($div);
            data.context.html($div);
            $i.hide().fadeIn('slow');
            waitForThumb(data.result.key, data.context);
            data.context.after('<input type="hidden" name=photo_id value="' + data.result.photo_id + '">');
        }
    });

    $('#album-save').on('click', function(e) {
        var $form = $('#album-save-form');
        var $target = $(e.target);
        var data = $form.serializeObject();
        alert(data);
        data = JSON.stringify(data);
        var url = $form.attr('action');
        var method = $form.attr('method');
        var $i = $('<i/>');
        $i.addClass("fa fa-refresh fa-spin");
        $target.append($i);
        $.ajax({
            'url': url,
            'method': method,
            'contentType': 'application/json; charset=utf-8',
            'dataType': 'json',
            'data': data
        }).done(function(data){
            $i.fadeOut('slow', function(){
                $i.remove();
            });
            if (data.status === 'success') {
                window.location.href='/album/view/' + data.album_id;
            } else if (data.status === 'error') {
                showMessage(data.message);
            }
        }).error(function(data){
            $i.remove();
        });
    });
}

function waitForThumb(key, context) {
    $.ajax({
            url: '/photo/view',
            data: {'key': key + ".thumb"},
            type:'HEAD',
            error: function() {
        },
        success: function() {
            context.html('<img src="' + sdcdn + '/' + key + '.thumb">');
        }
    });
}

function previewHandler() {
    $('#collage').collagePlus({
        'fadeSpeed': 2000,
        'targetHeight': 200,
        'allowPartialLastRow' : true
    });
    $('#collage').on('click', 'img', function (e) {
        e.preventDefault();
        var photo_id = $(e.target).attr('photo_id');
        window.location.href = '/photo/view/' + photo_id;
    });
}

function photoHandler() {
    var magic_padding = 144; //72 px is nav height. 50(nav height) + 2 (border) + 20 (nav margin)
    var $img = $('#photo-container img');
    var img_width = $img.attr('width');
    var img_height = $img.attr('height');
    var screen_width = $(window).width();
    var screen_height = $(window).height();

    var max_height = screen_height - magic_padding;
    var max_width = $('.container').width();

    //if max_width/img_width > max_height/img_height implies => Streched wide
    if (max_width * img_height > max_height * img_width) { //Streched wide
        $img.css({
            height: max_height,
            width: 'auto'
        });
    } else {
    }
}

function profileHandler() {
//    $('#album-save').on('click', function(e){
//        $('#upload-profile-photo-modal').modal('hide');
//    });
    $('#profile-edit-save').on('click', function(e){
        if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
        var $form = $(this).parents('form');
        var $target = $(e.target);
        var data = $form.serializeObject();
        data = JSON.stringify(data);
        var url = $form.attr('action');
        var method = $form.attr('method');
        var $i = $('<i/>');
        $i.addClass("fa fa-refresh fa-spin");
        $target.append($i);
        $.ajax({
            'url': url,
            'method': method,
            'contentType': 'application/json; charset=utf-8',
            'dataType': 'json',
            'data': data
        }).done(function(data){
            $i.fadeOut('slow', function(){
                $i.remove();
            });
            if (data.status === 'success') {
            } else if (data.status === 'error') {
                showMessage(data.message);
            }
            $('#profile-modal').modal('hide');
        });
    });
}