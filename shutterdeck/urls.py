from shutterdeck import views
from django.conf.urls import url, include

urlpatterns = [
    url(r'^$', views.index),

    url(r'^register/$', views.register),
    url(r'^login/$', views.login),
    url(r'^logout/$', views.logout),

    url(r'^home/$', views.home),

    url(r'^upload/$', views.upload),
    url(r'^photo/upload/$', views.upload),

    url(r'^album/view/(?P<album_id>[-\w+])$', views.view_album),
    url(r'^album/save/$', views.save_album),

    url(r'^photo/view/(?P<photo_id>[-\w]+)$', views.view_photo),
    url(r'^photo/save/$', views.save_photo),

    url(r'^profile/view/$', views.view_profile),
    url(r'^profile/save/$', views.save_profile),

    url(r'^albums/$', views.albums),
    url(r'^preview/$', views.preview),
]