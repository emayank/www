import json
from shutterdeck import helpers
from django.contrib import auth
from django.shortcuts import redirect
from common.response import Renderer, Success, Error
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required

from shutterdeck.models import Album
from shutterdeck.models import Photo


def index(request):
    if request.user.is_authenticated():
        return redirect('/home')
    return Renderer.render(request, 'index.html')


@csrf_protect
def register(request):
    if request.method == 'GET':
        return Renderer.render(request, 'register.html')
    elif request.method == 'POST' and request.is_ajax():
        params = request.body.decode()
        user_id = helpers.create_user(params)
        return Success()


@csrf_protect
def login(request):
    if request.method == 'GET':
        return Renderer.render(request, 'login.html')
    elif request.method == 'POST' and request.is_ajax():
        params = json.loads(request.body.decode())
        user = helpers.login(params)
        auth.login(request, user)
        return Success()


def logout(request):
    if request.method == 'GET':
        auth.logout(request)
        return redirect('/')


@login_required
def home(request):
    if request.method == 'GET':
        return Renderer.render(request, 'home.html')


@login_required
def upload(request):
    if request.method == 'GET':
        album_id = request.GET.get('album_id')
        context = dict(album_id=album_id)
        return Renderer.render(request, 'upload.html', context)


def view_album(request, album_id):
    if request.method == 'GET':
        context = dict(album_id=album_id)
        return Renderer.render(request, 'upload.html', context)


@login_required
def save_album(request):
    if request.is_ajax() and request.method == 'POST':
        params = request.body.decode()
        params = json.loads(params)
        album_id = params.get('album_id')
        if album_id is None:
            album = Album.objects.create_album(request.user.id)
            album_id = album.id
        photo_ids = params.get('photo_id')
        Photo.objects.filter(id__in=photo_ids).update(album_id=album_id)
        return Success(album_id=album_id)


def view_photo(request, photo_id):
    if request.method == 'GET':
        return Renderer.render(request, 'photo.html')


@login_required
def save_photo(request):
    pass


def view_profile(request):
    if request.method == 'GET':
        profile_id = request.GET.get('profile_id')
        if profile_id is None and not request.user.is_authenticated():
            return redirect('/login')
        return Renderer.render(request, 'profile.html')


@login_required
def save_profile(request):
    pass


def albums(request):
    if request.method == 'GET':
        profile_id = request.GET.get('profile_id')
        if profile_id is None and not request.user.is_authenticated():
            return redirect('/login')
        return Renderer.render(request, 'albums.html')


def preview(request):
    if request.method == 'GET':
        profile_id = request.GET.get('profile_id')
        if profile_id is None and not request.user.is_authenticated():
            return redirect('/login')
        return Renderer.render(request, 'preview.html')
