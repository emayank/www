from django.db import models
from django.contrib.auth.models import User


class AlbumManager(models.Manager):
    def create_album(self, user_id):
        album = self.create(user_id=user_id)
        return album


class Album(models.Model):
    user = models.ForeignKey(User)
    album_title = models.CharField(max_length=1024)
    album_desc = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)
    objects = AlbumManager()


class UserProfile(models.Model):
    user = models.ForeignKey(User)
    profile_album_id = models.IntegerField()
    profile_photo_id = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)


class PhotoManager(models.Manager):
    def create_photo(self, user_id):
        photo = self.create(user_id=user_id)
        return photo


class Photo(models.Model):
    user = models.ForeignKey(User)
    album = models.ForeignKey(Album)
    photo_width = models.SmallIntegerField()
    photo_height = models.SmallIntegerField()
    photo_title = models.CharField(max_length=1024)
    photo_desc = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)
    objects = PhotoManager()
