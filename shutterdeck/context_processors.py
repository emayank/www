from django.contrib.auth.models import User


def user_profile(request):
    profile = None
    profile_id = request.GET.get('profile_id')
    if profile_id is not None:
        profile = User.objects.get(id=profile_id)
    elif request.user.is_authenticated():
        profile = request.user
    return dict(profile=profile)
